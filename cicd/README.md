# Gitlab CI/CD supporting files

Folders:

- `scripts`: python scripts that are executed by gitlab jobs
- `templates`: jinja2 templates used for various purposes. Child Pipeline templates are here
- `tests`: Contains sub-folders that should match structure of inventories folder. Per site CI testing is here

Files:

- `email-permissions.txt`: list of email addresses to grant visibility/permission of CI test simulations
- `README.md`: This file
